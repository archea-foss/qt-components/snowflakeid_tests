##########################################################################
#
# Copyright (c) 2024 Erik Ridderby, ARCHEA.
# All rights reserved.
#
# This source code is licensed under both the 
# * BSD-3-Clause license with No Nuclear or Weapons use exception
#   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in 
#    the root directory of this source tree) 
#
# and the 
# 
# * GPLv3 
#   (found in the COPYING file in the root directory of this source tree).
# 
# You may select, at your option, one of the above-listed licenses.
# 
#
##########################################################################


QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  \
    SnowFlakeID/snowflakeid.cpp \
    SnowFlakeIDTests.cpp \
    test_main.cpp

HEADERS += \
    SnowFlakeID/SnowFlakeID_global.h \
    SnowFlakeID/snowflakeid.h \
    SnowFlakeIDTests.h

SUBDIRS += \
    SnowFlakeID/SnowFlakeID.pro

unix|win32: LIBS += -L$$PWD/../lib/QProQuint/ -lQProQuint

INCLUDEPATH += $$PWD/../include/QProQuint
DEPENDPATH += $$PWD/../include/QProQuint

DISTFILES += \
    README.md
