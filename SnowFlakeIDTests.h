/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the 
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in 
 *   the root directory of this source tree) 
 *
 * and the 
 * 
 * * GPLv3 
 *   (found in the COPYING file in the root directory of this source tree).
 * 
 * You may select, at your option, one of the above-listed licenses.
 * 
 */
 

#ifndef SNOWFLAKEIDTESTS_H
#define SNOWFLAKEIDTESTS_H

#include <QObject>

class SnowFlakeIDTests : public QObject
{
        Q_OBJECT

    public:
        SnowFlakeIDTests() {}
        ~SnowFlakeIDTests() {}

    private slots:

        void timeStampTests();
        void test_case1();
        void hashTests();
        void testEquality();
        void testTransferUsingHashValue();
        void testTransferUsingHashString();
        void testTransferUsingBrokenHashString();
        void testCreateFromString();
        void testCreateFromStringWithNullFields();
        void testVariantMap();


};


#endif // SNOWFLAKEIDTESTS_H
