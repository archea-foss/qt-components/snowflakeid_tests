/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the 
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in 
 *   the root directory of this source tree) 
 *
 * and the 
 * 
 * * GPLv3 
 *   (found in the COPYING file in the root directory of this source tree).
 * 
 * You may select, at your option, one of the above-listed licenses.
 * 
 */
 

#include <QtTest>

#include "SnowFlakeIDTests.h"

#include "SnowFlakeID/snowflakeid.h"



void SnowFlakeIDTests::timeStampTests()
{

    QDateTime orgTime = QDateTime::currentDateTimeUtc();

    quint64   timeCode = orgTime.toMSecsSinceEpoch();

    // Setup the result
    QTimeZone   tz(QTimeZone::UTC);
    QDateTime timeStamp = QDateTime::fromMSecsSinceEpoch(timeCode, tz);

    QVERIFY(orgTime == timeStamp);


}

void SnowFlakeIDTests::test_case1()
{

    quint32 context1 = SnowFlakeID::GenerateContext();

    SnowFlakeID id1;

    // qDebug() << "ID1 as string: " << id1.toString();
    QVERIFY(id1.toString() == "(null) (null) (null) (null)");

    // qDebug() << "Is ID1 valid?" << id1.isValid();
    QVERIFY(id1.isValid() == false);
    QVERIFY(id1.isNull() == true);


    id1 = SnowFlakeID::FromContext(context1);

    // qDebug() << "Is ID1 valid?" << id1.isValid();

    QVERIFY(id1.isValid() == true);
    QVERIFY(id1.isNull() == false);

}

void SnowFlakeIDTests::hashTests()
{
    quint32 context1 = SnowFlakeID::GenerateContext();

    SnowFlakeID id1 = SnowFlakeID::FromContext(context1);

    QHash<quint128, QString>    objects;

    QString fid = id1.toString();
    // qDebug() << "Snow flake ID is: " << fid;

    objects.insert(id1.hash(), fid);

    qDebug() << "From Hash Table:" << objects.value(id1.hash());;
    QVERIFY(objects.value(id1.hash()) == fid);

}

void SnowFlakeIDTests::testEquality()
{
    quint32 context1 = SnowFlakeID::GenerateContext();
    SnowFlakeID id1 = SnowFlakeID::FromContext(context1);
    SnowFlakeID id2(id1);
    SnowFlakeID id3;

    // qDebug() << "Snow flake ID1 is: " << id1;
    // qDebug() << "Snow flake ID2 is: " << id2;
    // qDebug() << "Snow flake ID3 is: " << id3;
    // qDebug() << "ID1 == ID2: " << (id1 == id2);
    // qDebug() << "ID1 == ID3: " << (id1 == id3);
    // qDebug() << "ID2 == ID3: " << (id2 == id3);

    QVERIFY(id1 == id2);
    QVERIFY( (id1 == id3) == false);
    QVERIFY( (id2 == id3) == false);

    id3 = id1;

    // qDebug() << "Snow flake ID1 is: " << id1;
    // qDebug() << "Snow flake ID2 is: " << id2;
    // qDebug() << "Snow flake ID3 is: " << id3;
    // qDebug() << "ID1 == ID2: " << (id1 == id2);
    // qDebug() << "ID1 == ID3: " << (id1 == id3);
    // qDebug() << "ID2 == ID3: " << (id2 == id3);

    QVERIFY(id1 == id2);
    QVERIFY(id1 == id3);
    QVERIFY(id2 == id3);


    SnowFlakeID empty1;
    SnowFlakeID empty2;
    // qDebug() << "Empty flake == empty falke? " << (empty1 == empty2);
    QVERIFY(empty1 == empty2);

}

void SnowFlakeIDTests::testTransferUsingHashValue()
{
    quint32 context1 = SnowFlakeID::GenerateContext();
    SnowFlakeID id1 = SnowFlakeID::FromContext(context1);

    qDebug() << "Transfer using hash-value";

    qDebug() << id1.hashString();
    SnowFlakeID id4 = SnowFlakeID::FromHash(id1.hash());
    qDebug() << "Snow flake ID1 is: " << id1;
    qDebug() << "Snow flake ID4 is: " << id4;

    QVERIFY(id1 == id4);
    QVERIFY(id1.toString() == id4.toString());
    QVERIFY(id1.hash() == id4.hash());
    QVERIFY(id1.hashString() == id4.hashString());

    SnowFlakeID id5 = SnowFlakeID::FromHash(0x00);
    QVERIFY(id5.isValid() == false);
    QVERIFY(id5.isNull() == true);
    qDebug() << "0x00 string: " << id5.toString();
    QVERIFY(id5.toString() == "1970-01-01 00:00:00.000 (null) (null)");



}

void SnowFlakeIDTests::testTransferUsingHashString()
{
    qDebug() << "Transfer using hash-string";

    quint32 context1 = SnowFlakeID::GenerateContext();
    SnowFlakeID id1 = SnowFlakeID::FromContext(context1);
    qDebug() << "Snow flake ID1 is   : " << id1;
    qDebug() << "           ID1 hash : " << id1.hashString();

    SnowFlakeID id5 = SnowFlakeID::FromHash(id1.hashString());
    qDebug() << "Snow flake ID5 is   : " << id5;
    qDebug() << "           ID5 hash : " << id5.hashString();

    QVERIFY(id1 == id5);
    QVERIFY(id1.toString() == id5.toString());
    QVERIFY(id1.hash() == id5.hash());
    QVERIFY(id1.hashString() == id5.hashString());

}

void SnowFlakeIDTests::testTransferUsingBrokenHashString()
{
    qDebug() << "Transfer using broken hash-string";
    // qDebug() << "OK: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44df13:75c11230ce09d350"));
    // qDebug() << "OK: " << SnowFlakeID::FromHash(QStringLiteral("0000018dFe44df13:75c11230ce09d350"));
    // qDebug() << "OK: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44dF13:75c11230CE09d350"));

    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d350")).toString() ==
            "2024-03-02 08:25:42.931 lilad-damub suman-tatib");

    QVERIFY(SnowFlakeID::FromHash(QStringLiteral("0000018dFe44df13:75c11230ce09d350")).toString() ==
            "2024-03-02 08:25:42.931 lilad-damub suman-tatib");

    QVERIFY(SnowFlakeID::FromHash(QStringLiteral("0000018dfe44dF13:75c11230CE09d350")).toString() ==
            "2024-03-02 08:25:42.931 lilad-damub suman-tatib");

    // qDebug() << "Null: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44df13_75c11230ce09d350"));
    // qDebug() << "Null: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44df13:75c11230ce09d35"));
    qDebug() << "Null: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44df1:75c11230ce09d350"));
    // qDebug() << "Null: " << SnowFlakeID::FromHash(QStringLiteral("0000018dfe44df13:75c11230ce09d35x"));

    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df13_75c11230ce09d350")).toString() == "(null) (null) (null) (null)");
    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df13_75c11230ce09d350")).isValid() == false);
    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df13_75c11230ce09d350")).isNull() == true);

    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35")).toString() == "(null) (null) (null) (null)");
    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35")).isValid() == false);
    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35")).isNull() == true);

    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df1:75c11230ce09d350")).toString() == "(null) (null) (null) (null)");
    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df1:75c11230ce09d350")).isValid() == false);
    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df1:75c11230ce09d350")).isNull() == true);


    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35x")).toString() == "(null) (null) (null) (null)");
    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35x")).isValid() == false);
    QVERIFY(SnowFlakeID::FromHash(("0000018dfe44df13:75c11230ce09d35x")).isNull() == true);

}

void SnowFlakeIDTests::testCreateFromString()
{
    qDebug() << "Create from string:";
    SnowFlakeID flake;

    // qDebug() << "Ok: " << SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub suman-tatib");
    // qDebug() << "Ok: " << SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub SUMAN-TATIB");

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub suman-tatib");
    QVERIFY(flake.isValid() == true);
    QVERIFY(flake.isNull() == false);
    QVERIFY(flake.toString() == "2024-03-02 08:25:42.931 lilad-damub suman-tatib");

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub SUMAN-TATIB");
    QVERIFY(flake.isValid() == true);
    QVERIFY(flake.isNull() == false);
    QVERIFY(flake.toString() == "2024-03-02 08:25:42.931 lilad-damub suman-tatib");

    // flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub babab-babab");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();

    // Context or Random can not be 0x00, that means null.

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub babab-babab");
    QVERIFY(flake.isValid() == false);
    QVERIFY(flake.isNull() == true);
    QVERIFY(flake.toString() == "2024-03-02 08:25:42.931 lilad-damub (null)");


    // flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 babab-babab suman-tatib");
    // qDebug() << "OK: " << flake << ", isValid: " << flake.isValid();

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 babab-babab suman-tatib");
    QVERIFY(flake.isValid() == false);
    QVERIFY(flake.isNull() == true);
    QVERIFY(flake.toString() == "2024-03-02 08:25:42.931 (null) suman-tatib");

    // flake = SnowFlakeID::FromString("babab-babab babab-babab babab-babab suman-tatib");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();

    flake = SnowFlakeID::FromString("babab-babab babab-babab babab-babab suman-tatib");
    QVERIFY(flake.isValid() == false);
    QVERIFY(flake.isNull() == true);
    QVERIFY(flake.toString() == "(null) (null) (null) suman-tatib");

    // flake = SnowFlakeID::FromString("suman-tatib babab-babab babab-babab suman-tatib");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();


    flake = SnowFlakeID::FromString("suman-tatib babab-babab babab-babab suman-tatib");
    QVERIFY(flake.isValid() == false);
    QVERIFY(flake.isNull() == true);
    QVERIFY(flake.toString() == "(null) (null) (null) suman-tatib");


}

void SnowFlakeIDTests::testCreateFromStringWithNullFields()
{
    qDebug() << "Create from string:";
    SnowFlakeID flake;

    qDebug() << "From string with null-fields";

    flake = SnowFlakeID::FromString("(null) 08:25:42.931 lilad-damub suman-tatib");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();
    QVERIFY(flake.isValid() == false);
    QVERIFY(flake.isNull() == true);
    QVERIFY(flake.toString() == "(null) 08:25:42.931 lilad-damub suman-tatib");

    flake = SnowFlakeID::FromString("2024-03-02 (null) lilad-damub suman-tatib");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();
    QVERIFY(flake.isValid() == false);
    QVERIFY(flake.isNull() == true);
    QVERIFY(flake.toString() == "2024-03-02 (null) lilad-damub suman-tatib");

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 (null) suman-tatib");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();
    QVERIFY(flake.isValid() == false);
    QVERIFY(flake.isNull() == true);
    QVERIFY(flake.toString() == "2024-03-02 08:25:42.931 (null) suman-tatib");

    flake = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub (null)");
    // qDebug() << "Invalid: " << flake << ", isValid: " << flake.isValid();
    QVERIFY(flake.isValid() == false);
    QVERIFY(flake.isNull() == true);
    QVERIFY(flake.toString() == "2024-03-02 08:25:42.931 lilad-damub (null)");

}

void SnowFlakeIDTests::testVariantMap()
{
    qDebug() << "VariantMaps: ";
    SnowFlakeID id7;

    // qDebug() << "Empty flake: " << id7.toVariant();

    id7 = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub suman-tatib");
    // qDebug() << "OK flake: " << id7.toVariant();
    // qDebug() << "Same as source? " << (SnowFlakeID::FromVariantMap(id7.toVariant()) == id7) << ", result: " << SnowFlakeID::FromVariantMap(id7.toVariant()).toString();

    QVERIFY(id7.isValid());
    QVERIFY(SnowFlakeID::FromVariantMap(id7.toVariant()) == id7);

    id7 = SnowFlakeID::FromString("2024-03-02 08:25:42.931 lilad-damub SUMAN-TATIB");
    // qDebug() << "OK flake: " << id7.toVariant();
    // qDebug() << "Same as source? " << (SnowFlakeID::FromVariantMap(id7.toVariant()) == id7) << ", result: " << SnowFlakeID::FromVariantMap(id7.toVariant()).toString();

    QVERIFY(id7.isValid());
    QVERIFY(SnowFlakeID::FromVariantMap(id7.toVariant()) == id7);

    id7 = SnowFlakeID::FromString("suman-tatib babab-babab babab-babab suman-tatib");
    // qDebug() << "NULL flake: " << id7.toVariant();
    // qDebug() << "Same as source? " << (SnowFlakeID::FromVariantMap(id7.toVariant()) == id7) << ", result: " << SnowFlakeID::FromVariantMap(id7.toVariant()).toString();

    QVERIFY(id7.isNull());
    QVERIFY(SnowFlakeID::FromVariantMap(id7.toVariant()) == id7);

}
